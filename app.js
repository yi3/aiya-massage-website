/**
 * AIYA Massage Website
 * @namespace AIYA
 * @author Wesley B <wesleyb@protonmail.com>
 * @todo Test and handle server errors (ex: 404).
 */

///////////////////////////////////////////////////////////////////////////////
// Dependencies
///////////////////////////////////////////////////////////////////////////////

//
// Internal
//

/**
 * Node.js File System
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [node: fs]{@link https://nodejs.org/api/fs.html}
 */
const fs = require( 'fs' );

/**
 * Extend one object with another
 * @memberOf AIYA
 * @constant
 * @type {Function}
 * @see [node: util]{@link https://github.com/joyent/node/blob/master/lib/util.js}
 */
const extend = require( 'util' )._extend;

//
// External
//

/**
 * Web framework for Node.js
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: express]{@link https://www.npmjs.com/package/express}
 */
const express = require( 'express' );

/**
 * A [Handlebars](http://handlebarsjs.com/) view engine for Express
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: express-handlebars]{@link https://www.npmjs.com/package/express-handlebars}
 * @todo Consider replacing this with [npm: consolidate]{@link https://www.npmjs.com/package/consolidate}
 * @todo Strip whitespace from templates before compilation (requires gulp and a new directory) because
 *       [Handlebars will not clean whitespace for us]{@link https://github.com/wycats/handlebars.js/issues/867}
 */
const exphbs = require( 'express-handlebars' );

/**
 * A Gzip Compression "Middleware" for Express
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: compression]{@link https://www.npmjs.com/package/compression}
 * @see [Do not forget to use Gzip for Express.js]{@link http://inspiredjw.com/do-not-forget-to-use-gzip-for-express/}
 */
var compress = require( 'compression' );

/**
 * A Javascript implementation of the DOM
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: node-jsdom]{@link https://www.npmjs.com/package/node-jsdom}
 */
// XXX: Installation of `node-jsdom` is broken 2015-09-26
// const jsdom = require( 'node-jsdom' );

/**
 * Send e-mail from Node.js
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: nodemailer]{@link https://www.npmjs.com/package/nodemailer}
 */
const nodemailer = require( 'nodemailer' );

///////////////////////////////////////////////////////////////////////////////
// Setup
///////////////////////////////////////////////////////////////////////////////

/**
 * AIYA Massage Website Server
 * @memberOf AIYA
 * @namespace AIYA.Server
 * @author Wesley B <wesleyb@protonmail.com>
 */

/**
 * Custom constant values for this file
 * @memberOf AIYA.Server
 * @constant
 * @type {Object}
 */
var constants = {
  /**
   * The path to the server root
   * @type {String}
   */
  ROOT: __dirname + '/'
}

/**
 * Common or important time measurements in seconds
 * @memberOf AIYA.Server.constants
 * @enum {String}
 */
constants.TIME = {
  /** The number of seconds in one year */
  year: 31536000000
};

/**
 * Common or important emails
 * @memberOf AIYA.Server.constants
 * @enum {String}
 */
constants.EMAIL = {
  /** The e-mail address of the webserver */
  server: 'Aiya Massage Website <server@aiyamassage.com>',

  /** The e-mail address of the webmaster */
  webmaster: 'Wesley B <wesleyb@protonmail.com>'
};

/**
 * Common or important paths
 * @memberOf AIYA.Server.constants
 * @enum {String}
 */
constants.PATH = {
  /** The path to the public directory */
  public: constants.ROOT + 'public/',

  /** The name of the calendar HTML file */
  calendarMarkup: 'public/calendar.html',

  /** The name of the calendar CSS file */
  calendarStyleseet: 'public/stylesheets/calendar.css',

  /** The URL of the remote calendar interface */
  calendarRemoteURL: 'https://www.google.com/calendar/embed?src=50icm7k4c88bnac6gtm7m8sli4%40group.calendar.google.com&ctz=America/Chicago'
};

/**
 * Reusable transporter object using SMTP transport
 * @memberOf AIYA.Server
 * @constant
 * @type {Class}
 */
var transporter = nodemailer.createTransport( {
  service: 'gmail',
  auth: {
    user: 'aiyamassage@gmail.com',
    pass: 'AIYAlj707'
  }
} );

///////////////////////////////////////////////////////////////////////////////
// Application
///////////////////////////////////////////////////////////////////////////////

/**
 * Express Application
 * @memberOf AIYA.Server
 * @constant
 * @type {Class}
 * @see [express: app]{@link http://expressjs.com/4x/api.html#app}
 */
var app = express();

//
// Server
//

app.set( 'port', ( process.env.PORT || 5000 ) );

//
// Compression
//

app.use( compress() );

//
// Middleware
//

// Built-in middleware
// @see [express: middleware.built-in]{@link http://expressjs.com/guide/using-middleware.html#middleware.built-in}
app.use( express.static( constants.PATH.public, { maxAge: constants.TIME.year } ) );

//
// Engine
//

// Usage of "Express Handlebars" templating engine
// @see [npm: express-handlebars: usage]{@link https://www.npmjs.com/package/express-handlebars#user-content-basic-usage}
app.engine( 'hbs', exphbs( {
  defaultLayout: 'no-sidebar',
  extname: 'hbs',
  helpers: {
    ifeq: function(v1, v2, options) {
      // Using Regex this way allows a user to test "or" with `|`
      if ( v1.match( new RegExp( '^' + v2 + '$' ) ) ) {
        return options.fn( this );
      } else {
        return options.inverse( this );
      }
    }
  },
  layoutsDir: 'views/layouts/',
  partialsDir: 'views/partials/'
} ) );
app.set( 'view engine', 'hbs' );

//
// Routes
//

/**
 * AIYA Massage Website Routes
 * @memberOf AIYA
 * @namespace AIYA.Routes
 */

/**
 * HTML pages
 * @memberOf AIYA.Routes
 * @var {Route} /*.html
 */
app.get( '/*\.html', function ( request, response, next ) {
  var path = getPathFromRequest( request );

  switch ( path ) {
  case 'about.html':
    response.redirect( 301, '/' );
    break;

  case 'contact.html':
    response.redirect( 301, '/locations' );
    break;

  case 'services.html':
    response.redirect( 301, '/disciplines' );
    break;

  default:
    response.sendFile( constants.PATH.public + path, function ( error, html ) {
      if ( error ) {
        console.error( error );
        next();
      } else {
        response.send( html );
      }
    } );
    break;
  }
} );

/**
 * All other pages
 * @memberOf AIYA.Routes
 * @var {Route} /*
 */
app.get( '/*', function ( request, response, next ) {
  var path = getPathFromRequest( request );
  var metadata = getData( 'meta' );
  var options = {};

  if ( path === '' ) {
    path = 'welcome';
  }

  if ( metadata[ path ] ) {
    options.metadata = metadata[ path ];
  }

  // Certain paths share layouts
  switch ( path ) {
    case 'affiliates':
    case 'disciplines':
    case 'jobs':
    case 'welcome':
      options.layout = 'homepage';
      break;

    case 'aiya-custom-massage':
      options.layout = 'left-sidebar';
      break;

    case 'coming-soon':
      options.layout = 'placeholder';
      break;

    default:
      options.layout = 'no-sidebar';
      break;
  }

  // Certain paths share data
  switch ( path ) {
    case 'jobs':
      options.jobs = getData( 'jobs' );
      break;

    case 'affiliates':
      options.affiliates = getData( 'affiliates' );
      break;

    default:
      break;
  }
  options.disciplines = getData( 'disciplines' );


  response.render( path, options, function ( error, html ) {
    if ( error ) {
      console.error( error );
      next();
    } else {
      response.send( html );
    }
  } );
} );

/**
 * Page not found
 * @memberOf AIYA.Routes
 * @var {Route} /errors/404
 */
app.use( function ( request, response, next ) {
  var details = {
    url: request.originalUrl
  };
  var options = {
    layout: 'homepage'
  };

  response.status( 404 );
  response.render( 'errors/404', options, function ( error, html ) {
    if ( error ) {
      // Somebody should know this happenned
      console.error( 'Reporting 400 to user because:', error );
      sendErrorEmail( error, 400, details );
    } else {
      response.send( html );
    }
  } );
} );

/**
 * Page not found
 * @memberOf AIYA.Routes
 * @var {Route} /errors/500
 */
app.use( function ( error, request, response, next ) {
  var details = {
    url: request.originalUrl
  };
  var options = {
    layout: 'homepage'
  };

  response.status( error.status || 500 );
  response.render( 'errors/500', options );

  // Somebody should know this happenned
  console.error( 'Reporting 500 to user because:', error );
  sendErrorEmail( error, 500, details );
} );

//
// Logging
//

app.listen( app.get( 'port' ), function () {
  console.log( 'Node app is running on port: ', app.get( 'port' ) );
} );

//
// Generation
//

generateGoogleCalendar( constants.PATH.calendarRemoteURL, constants.PATH.calendarMarkup, constants.PATH.calendarStylesheet );

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////

/**
 * Get the path from a request
 * @memberOf AIYA
 * @param {String} request - The request to get the path of
 * @return {String}
 */
function getPathFromRequest( request ) {
  return request.originalUrl.replace( /^\//, '' );
}

/**
 * Get the JSON model that matches the specified data name
 * @memberOf AIYA
 * @param {String} name - The data name
 * @return {String}
 */
function getData( name ) {
  var filepath = constants.ROOT + 'data/' + name + '.json';
  var content;
  var json;

  try {
    content = fs.readFileSync( filepath, 'utf8' );
    json = JSON.parse( content );
  } catch ( err ) {
    // We know that some routes do not have models
    return;
  }

  return json;
}

/**
 * E-mail an error to the webmaster
 * @memberOf AIYA
 * @param {Error} error - The error that happenned
 * @param {number} [code] - The HTTP status code of the error
 * @param {mixed} [details] - Additional details about the error
 */
function sendErrorEmail( error, code, details ) {
  code = error.status || code || '???';
  error = stringify( error );
  details = stringify( details );

  var subject = 'Aiya Massage Website: ' + code + ' Error';
  var text = 'The Aiya Massage Website experienced a ' + code + ' error:' + "\n" + error;
  var html = 'The <b>Aiya Massage Website</b> experienced a ' + code + ' error:' + '<pre style="display: block"><code style="display: block">' + error + '</code></pre>';
  var mailOptions = {
    from: constants.EMAIL.server,
    to: constants.EMAIL.webmaster,
    subject: subject,
    text: text,
    html: html
  };

  if ( details ) {
    mailOptions.text += "\n" + 'These additional details were also given:' + "\n" + details;
    mailOptions.html += 'These additional details were also given:' + '<pre style="display: block"><code style="display: block">' + details + '</code></pre>';
  }

  // Somebody should know this happenned
  transporter.sendMail( mailOptions, function ( error, info ) {
    console.log( 'E-mail sent: ' + info.response );
  } );
}

/**
 * Make a string safely out of any given value
 * @memberOf AIYA
 * @param {Error} error - The error that happenned
 * @param {number} [code] - The HTTP status code of the error
 * @param {mixed} [details] - Additional details about the error
 */
function stringify( value ) {
  if ( value instanceof Error ) {
    value = value.toString();
  } else if ( typeof value === 'function' ) {
    value = value.toString();
  } else if ( typeof value !== 'string' ) {
    value = JSON.stringify( value );
  }

  return value;
}

/**
 * Write a file to the file system with given content
 * @memberOf AIYA
 * @param  {String} path - The path of the file to write
 * @param  {String} content  - The content of the file to write
 */
function writeFile( path, content ) {
  var buffer = new Buffer( content );

  fs.open( path, 'w', function ( err, fd ) {
    if ( err ) {
      throw 'Error opening file: ' + err;
    }

    fs.write( fd, buffer, 0, buffer.length, null, function ( err ) {
      if ( err ) {
        throw 'Error writing file: ' + err;
      }
      fs.close( fd, function () {
        console.log( 'Wrote file: ' + path );
      } );
    } );
  } );
}

/**
 * Create a link to a new stylesheet
 * @memberOf AIYA
 * @param  {String} document - The document to add the link to
 * @param  {String} attributes - A dictionary of the attributes to set
 */
function addLinkToStylesheet( document, attributes ) {
  var head = document.getElementsByTagName( 'head' )[ 0 ];
  var link = document.createElement( 'link' );

  attributes = extend( {
    type: 'text/css',
    rel: 'stylesheet'
  }, attributes );

  setAttributes( link, attributes );

  head.appendChild( link );
}

/**
 * Create an object with properties from a given URL
 * @memberOf AIYA
 * @param  {String} URL - The URL to build the object from
 * @param  {String} document - A document to use for building the object with DOM
 */
function createURLObject( URL, document ) {
  var anchor = document.createElement( 'a' );

  anchor.href = URL;

  return {
    protocol: anchor.protocol, // http:
    hostname: anchor.hostname, // domain.com
    port: anchor.port, // 3000
    pathname: anchor.pathname, // /path/to/something
    path: anchor.pathname.match( /(.*)\/.*/ )[ 1 ], // /path/to
    search: anchor.search, // ?query=String
    hash: anchor.hash, // #fragment
    host: anchor.host // domain.com:3000
  };
}

/**
 * Set attributes for an HTML element
 * @memberOf AIYA
 * @param  {HTMLElement} element - The element to set the attributes of
 * @param  {String} attributes - A dictionary of attributes to set
 */
function setAttributes( element, attributes ) {
  for ( var key in attributes ) {
    element.setAttribute( key, attributes[ key ] );
  }
}

/**
 * Create a local version of a remote Google calendar
 * @memberOf AIYA
 * @param  {String} url          - The URL of the remote calendar
 * @param  {String} filename     - The name of the local calendar file
 * @param  {String} [stylesheet] - The name of the stylesheet for the local calendar
 */
function generateGoogleCalendar( url, filename, stylesheet ) {
  // XXX: Installation of `node-jsdom` is broken 2015-09-26
  return;

  jsdom.env( {
    url: url,
    done: function ( errs, window ) {
      var document = window.document;
      var doctype = getDoctype( document );
      var URL = createURLObject( url, document );
      var content = '';

      if ( stylesheet ) {
        addLinkToStylesheet( document, {
          href: stylesheet
        } );
      }

      console.log( 'doctype', doctype );

      content = doctype + document.documentElement.outerHTML;
      content = content.replace( '"/"', '"//' + URL.host + '/"' );
      content = content.replace( '"images/"', '"//' + URL.host + URL.path + '/images/"' );

      writeFile( filename, content );
    }
  } );
}

/**
 * Get the doctype of a Document
 * @memberOf AIYA
 * @param {Document} document - The document to get the doctype of
 * @return {String}
 */
function getDoctype( document ) {
  return '<!DOCTYPE ' +
    document.doctype.name +
    ( document.doctype.publicId ? ' PUBLIC "' + document.doctype.publicId + '"' : '' ) +
    ( document.doctype.systemId ? ' "' + document.doctype.systemId + '"' : '' ) + '>';
}