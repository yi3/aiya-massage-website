## Aiya Massage Website - Branching

This branch naming structure is used by app developers at [goldenfrog.com](https://ww.goldenfrog.com/) and is inspired by [GitFlow](http://nvie.com/posts/a-successful-git-branching-model/).

### Reserved Branches

- `master`
    1. Merge completed work __to here__ from a `release/*` branch.
    2. Merge completed work __from here__ to heroku `master` branch.
- `develop`
    1. Merge completed work __from here__ into a prefixed branch:
        - `feature/*`
        - `bugfix/*`
        - `release/*`
    2. Merge completed work __to here__ from a prefixed branch:
        - `feature/*`
        - `bugfix/*`

### Branch Types & Naming

- `feature/*`
    1. Branch __from `develop`__.
    2. Create a new feature.
    3. Merge __into `develop`__.
- `bugfix/*`
    1. Branch __from `develop`__.
    2. Fix a bug.
    3. Merge __into `develop`__.
- `release/*`
    1. Branch __from `develop`__.
    2. Merge __into `master`__.
    3. Tag the commit with affixed name `*`.
- `hotfix/*`
    1. Branch __from a `release/*`__.
    2. Fix a bug.
    3. Merge __into `release/*`__.
