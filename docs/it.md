# Aiya Massage Website - Information Technology

## Domain E-Mail Address Management

See [Trello: Aiya Massage: Server - Emails](https://trello.com/c/Ir40J1zh/).

## Google Calendar Sharing

Sharing Instructions
: [Google: Support: Calendar: "Share your calendar with someone"](https://support.google.com/calendar/answer/143754)

Current Embedded Calendar Link
: [../app.js](../app.js) _(search for `https://www.google.com/calendar/`)_
