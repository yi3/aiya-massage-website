# Aiya Massage Website - Deploy

To deploy, one **must** first have built the project.

## Build Project

See [./build.md](./build.md).

## Staging Environment

You **must** ensure that local branch to deploy is back-merged with remote repo `staging` branch `master`. **Then**, you can push branch to remote repo `staging` branch `master`.

### Requirements

- Git must have registered the [remote Heroku staging repo](https://git.heroku.com/aiya-massage-staging.git)* as remote repo `staging`.

\* If this links is no longer valid, find the latest link at [Heroku App Settings for `aiya-massage-staging`]https://dashboard.heroku.com/apps/aiya-massage-staging/settings).

### Instructions

1. Try to push the current branch to remote repo `staging` branch `master`:

    ```
    git push staging <current_branch>:master
    ```

    **If**, either of these message blocks are received received:
    
    ```
    error: src refspec release/0.0.1 does not match any.
    error: failed to push some refs to 'https://git.heroku.com/aiya-massage-staging.git'
    ```
    
    ```
    Everything up-to-date
    ```
    
    **Then**, perform step 2.
    
    **Otherwise**, _if no error message is presented_, skip step 2.

2. Back-merge remote repo `staging` branch `master` into local branch to deploy.

    ```
    git checkout <current_branch>
    git merge staging/master
    ...
    git commit
    ```

    **Then**, _if no error message is presented_, repeat step 1.
    
    **Otherwise**, solve the error.

3. Load app at either of the following web addresses:

    - [staging.aiyamassage.com](http://staging.aiyamassage.com/)
    - [aiya-massage-staging.herokuapp.com](https://aiya-massage-staging.herokuapp.com/)

## Production Environment

You **must** ensure that local branch to deploy is back-merged with remote repo `production` branch `master`. **Then**, you can push branch to remote repo `production` branch `master`.

### Requirements

- Git must have registered the [remote Heroku production repo](https://git.heroku.com/aiya-massage-production.git)* as remote repo `production`.

\* If this links is no longer valid, find the latest link at [Heroku App Settings for `aiya-massage-production`]https://dashboard.heroku.com/apps/aiya-massage-production/settings).

### Instructions

1. Try to push the branch to remote repo `production`:

    ```
    git push production <current_branch>:master
    ```

    **If**, either of these message blocks are received received:
    
    ```
    error: src refspec release/0.0.1 does not match any.
    error: failed to push some refs to 'https://git.heroku.com/aiya-massage-production.git'
    ```
    
    ```
    Everything up-to-date
    ```
    
    **Then**, perform step 2.
    
    **Otherwise**, _if no error message is presented_, skip step 2.

2. Back-merge remote repo `production` branch `master` into local branch to deploy.

    ```
    git checkout <current_branch>
    git merge production/master
    ...
    git commit
    ```

    **Then**, _if no error message is presented_, repeat step 1.
    
    **Otherwise**, solve the error.

3. Load app at either of the following web addresses:

    - [www.aiyamassage.com](http://www.aiyamassage.com/)
    - [aiya-massage-production.herokuapp.com](https://aiya-massage-production.herokuapp.com/)