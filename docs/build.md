# Aiya Massage Website - Build

## Public Server / Hosting

Aiya Massage is hosted with Heroku. See app `aiya-massage` in [Heroku Dashboard for `aiya-massage` App](https://dashboard.heroku.com/apps/).

## Project Commands

### Requirements

- [Ruby](https://www.ruby-lang.org/) 1.9.3+
- [Node.js](https://nodejs.org/en/) 4.2.0+

### Install Dependencies

0. `gem install foreman`
0. `npm isntall`

### Build Project

1. `gulp`

#### Test on Local Server

#### Requirements

- "Install Dependencies"

#### Commands

_Start_ local server
: `foreman start web`

_Stop_ local server
: `[ctrl]` + `C` _(or quit terminal)_

#### Test on Public Server

#### Requirements

- "Install Dependencies"
- [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) _(formerly [Heroku Toolbelt](https://toolbelt.heroku.com/))_

#### Commands

_Start_ public server
: `git push heroku master`

_Stop_ public server
: `heroku ps:scale web=0`
