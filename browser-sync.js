module.exports = {
  debugInfo: true,
  files: [
    './public/**/*',
    './views/**/*.handlebars'
  ],
  ghostMode: {
    forms: true,
    links: true,
    scroll: true
  },
  proxy: 'localhost:5000'
};