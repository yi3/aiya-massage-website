<!--
DO NOT EDIT THE MARKUP DIRECTLY!
Instead, edit the `.md` Markdown file
of the same name in the same directory.
-->
### What is an Aiya Custom Massage?

Your certified therapist will learn about your body and your goals, and apply techniques accordingly. A custom massage should seamlessly integrate disciplines, as necessary, to provide the relaxation, relief, and response that your body needs.

A custom Aiya massage distinguishes itself with a unique range of applied techniques, instead of a conventional massage or a patchwork of common Western techniques. Our licensed applications range from the following disciplines: Swedish, Ashiatsu, Shiatsu, Thai, Foot Reflexology, and Tui Na. Applications of prenatal, cranial, and geriatric are also available.

Typical sessions start with...