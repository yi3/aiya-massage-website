<!--
DO NOT EDIT THE MARKUP DIRECTLY!
Instead, edit the `.md` Markdown file
of the same name in the same directory.
-->
## Privacy Policy

### Information Collected and Used

We may request and collect certain basic information from you on our web site. For example, when you use our contact form we may request your name, e-mail address &amp; phone number. Unless you choose to provide information to us, we will not obtain personally-identifying information about you when you visit our site, other than general information for web site performance assessment purposes such as the number of visitors to the different sections, the web address from where they linked to our site, the domain name, the date, time and the duration of their visit to help make our site the best it can be.

### Third Party Disclosure

Any of the basic information you choose to provide to us and that we collect from you is confidential and protected by us. We do not sell, trade or rent your information to others.

### Identity Protection

We have several internal controls in place to protect the collected information. We consider the information confidential and proprietary and treat it with the same care and confidentiality as we would with our own individual personal information.