<!--
DO NOT EDIT THE MARKUP DIRECTLY!
Instead, edit the `.md` Markdown file
of the same name in the same directory.
-->
## Aiya Massage Services

### Your Custom Massage
| Duration | Price |
| -: | -: |
| 60 min | $70.00 |
| 90 min | $100.00 |
| 120 min | $130.00 |

### Relaxation Massage
| Duration | Price |
| -: | -: |
| 60 min | $60.00 |
| 90 min | $90.00 |
| 120 min | $120.00 |

### Deep Tissue Massage
| Duration | Price |
| -: | -: |
| 60 min | $60.00 |
| 90 min | $90.00 |
| 120 min | $120.00 |

### Swedish Massage
| Duration | Price |
| -: | -: |
| 60 min | $60.00 |
| 90 min | $88.00 |

### Ashiatsu &amp; Shiatsu
| Duration | Price |
| -: | -: |
| 60 min | $60.00 |
| 90 min | $90.00 |

### Hand & Foot Reflexology
| Duration | Price |
| -: | -: |
| 30 min | $30.00 |
| 60 min | $50.00 |
| 90 min | $70.00 |
| 120 min | $90.00 |

### Thai Massage
| Duration | Price |
| -: | -: |
| 60 min | $60.00 |
| 90 min | $90.00 |

### Myofascial Release
| Duration | Price |
| -: | -: |
| 60 min | $100.00 |
| 90 min | $200.00 |

### Tuina Chinese Massage
| Duration | Price |
| -: | -: |
| 60 min | $60.00 |
| 90 min | $90.00 |