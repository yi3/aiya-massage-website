/**
 * AIYA Massage Website
 * @namespace AIYA
 * @author Wesley B <wesleyb@protonmail.com>
 */
'use strict';

///////////////////////////////////////////////////////////////////////////////
// Dependencies
///////////////////////////////////////////////////////////////////////////////

//
// Required
//

/**
 * The streaming build system
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp]{@link https://www.npmjs.com/package/gulp}
 */
var gulp = require( 'gulp' );

//
// Plugins
//

/**
 * Gulp plugin for debugging
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-debug]{@link https://www.npmjs.com/package/gulp-debug}
 */
var debug = require( 'gulp-debug' );

/**
 * Gulp plugin for (SASS)[http://sass-lang.com/]
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-sass]{@link https://www.npmjs.com/package/gulp-sass}
 */
var sass = require( 'gulp-sass' );

/**
 * Gulp plugin for (SASS)[http://sass-lang.com/]
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-sourcemaps]{@link https://www.npmjs.com/package/gulp-sourcemaps}
 */
var sourcemaps = require( 'gulp-sourcemaps' );

/**
 * Gulp plugin for (SASS)[http://sass-lang.com/]
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-autoprefixer]{@link https://www.npmjs.com/package/gulp-autoprefixer}
 */
var prefix = require( 'gulp-autoprefixer' );

/**
 * Markdown to HTML
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-markdown]{@link https://www.npmjs.com/package/gulp-markdown}
 */
var markdown = require( 'gulp-markdown' );

/**
 * Concatenate files
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-concat]{@link https://www.npmjs.com/package/gulp-concat}
 */
var concat = require( 'gulp-concat' );

/**
 * Rename files
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-rename]{@link https://www.npmjs.com/package/gulp-rename}
 */
var rename = require( 'gulp-rename' );

/**
 * Conditionally run a task
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-if]{@link https://www.npmjs.com/package/gulp-if}
 */
var gulpif = require( 'gulp-if' );

/**
 * Minify CSS
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: gulp-minify-css]{@link https://www.npmjs.com/package/gulp-minify-css}
 */
var minifyCss = require( 'gulp-minify-css' );

/**
 * Parse command-line arguments
 * @memberOf AIYA
 * @constant
 * @type {Class}
 * @see [npm: yargs]{@link https://www.npmjs.com/package/yargs}
 */
var argv = require( 'yargs' ).argv;

///////////////////////////////////////////////////////////////////////////////
// Setup
///////////////////////////////////////////////////////////////////////////////

/**
 * AIYA Massage Website File Processor
 * @memberOf AIYA
 * @namespace AIYA.Processor
 * @author Wesley B <wesleyb@protonmail.com>
 */

/**
 * Custom constant values for this file
 * @memberOf AIYA.Processor
 * @constant
 * @type {Object}
 */
var constants = {
  /**
   * The path to the server root
   * @type {String}
   */
  ROOT: './' // TODO: Can we use `__dirname + '/'` instead?
};

/**
 * Common or important paths
 * @memberOf AIYA.Processor.constants
 * @enum {String}
 */
constants.PATH = {
  /** The path to the assets directory */
  assets: constants.ROOT + 'assets/',

  /** The path to the public directory */
  public: constants.ROOT + 'public/',

  /** The path to the templates directory */
  templates: constants.ROOT + 'views/'
};

///////////////////////////////////////////////////////////////////////////////
// Tasks
///////////////////////////////////////////////////////////////////////////////

/**
 * Stylesheet: SASS Preprocessing & Precompiled Copy
 * @memberOf AIYA.Processor
 * @var {Task} stylesheet
 * @example
 * // Run task
 * `gulp stylesheet`
 */
gulp.task( 'stylesheet-sass', function () {
  gulp.src( constants.PATH.assets + 'stylesheets/**/*.scss' )
    // Initialize sourcemaps
    .pipe( gulpif( argv.development, sourcemaps.init() ) )
    // Run SASS (log errors)
    .pipe( sass( {
      outputStyle: 'compressed'
    } ).on( 'error', sass.logError ) )
    // Write sourcemaps
    .pipe( gulpif( argv.development, sourcemaps.write() ) )
    // Auto-prefix styles
    .pipe( prefix( 'last 1 version', '> 1%', 'ie 8', 'ie 7' ) )
    // Debug
    .pipe( debug( {
      title: 'Debug sass'
    } ) )
    // Save output
    .pipe( gulp.dest( constants.PATH.public + 'stylesheets/' ) );
} );
gulp.task( 'stylesheet-minify', function () {
  return gulp.src( constants.PATH.assets + 'stylesheets/precompiled/*.css' )
    // Perform task
    .pipe( minifyCss( {
      compatibility: 'ie8'
    } ) )
    // Debug
    .pipe( debug( {
      title: 'Debug stylesheet minify'
    } ) )
    .pipe( gulp.dest( constants.PATH.public + 'stylesheets' ) );
} );
gulp.task( 'stylesheet', [ 'stylesheet-sass', 'stylesheet-minify' ] );

/**
 * Markdown: Markdown to HTML
 * @memberOf AIYA.Processor
 * @var {Task} markdown
 * @example
 * // Run task
 * `gulp`
 */
gulp.task( 'markdown', function () {
  return gulp.src( constants.PATH.templates + '**/*.md' )
    // Perform task
    .pipe( markdown() )
    // Change extension
    .pipe( rename( function ( path ) {
      path.extname = '.hbs'
    } ) )
    // Debug
    .pipe( debug( {
      title: 'Debug markdown'
    } ) )
    // Save output
    .pipe( gulp.dest( constants.PATH.templates ) );
} );

/**
 * Javascript: Concatenate & Copy
 * @memberOf AIYA.Processor
 * @var {Task} javascript
 * @example
 * // Run task
 * `gulp`
 */
gulp.task( 'javascript-concatenate', function () {
  return gulp.src( [
      constants.PATH.assets + 'javascripts/jquery.min.js',
      constants.PATH.assets + 'javascripts/jquery.dropotron.min.js',
      constants.PATH.assets + 'javascripts/skel.min.js',
      constants.PATH.assets + 'javascripts/util.js',
      constants.PATH.assets + 'javascripts/main.js',
    ] )
    // Perform task
    .pipe( concat( 'scripts.js' ) )
    // Debug
    .pipe( debug( {
      title: 'Debug javascript concatenate'
    } ) )
    // Save output
    .pipe( gulp.dest( constants.PATH.public + 'javascripts/' ) );
} );
gulp.task( 'javascript-copy', function () {
  return gulp.src( constants.PATH.assets + 'javascripts/ie/*' )
    // Debug
    .pipe( debug( {
      title: 'Debug javascript copy'
    } ) )
    .pipe( gulp.dest( constants.PATH.public + 'javascripts/ie' ) );
} );
gulp.task( 'javascript', [ 'javascript-concatenate', 'javascript-copy' ] );

/**
 * Watch: Run Task on File Change
 * @memberOf AIYA.Processor
 * @var {Task} watch
 * @example
 * // Run task
 * 1. Change file
 * 2. Tasks are automatically run:
 *     - `sass`
 *     - `markdown`
 */
gulp.task( 'watch', function () {
  gulp.watch( constants.PATH.assets + 'stylesheets/**/*', [ 'stylesheet' ] );
  gulp.watch( constants.PATH.templates + 'partials/**/*.md', [ 'markdown' ] );
  gulp.watch( constants.PATH.assets + 'javascripts/**/*.js', [ 'javascript' ] );
} );

/**
 * Gulp: Default Task
 * @memberOf AIYA.Processor
 * @var {Task} default
 * @example
 * // Run task
 * `gulp`
 */
gulp.task( 'default', [ 'stylesheet', 'javascript', 'markdown', 'watch' ] );
