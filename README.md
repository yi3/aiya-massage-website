# Aiya Massage Website

This project is a static website with a Node+Express backend.

## Build & Deploy

- See [./docs/build.md](./docs/build.md).
- See [./docs/deploy.md](./docs/deploy.md).

## Project Access

Version Control
: https://bitbucket.org/yi3/aiya-massage-website/

Production
: http://www.aiyamassage.com/

Staging
: http://staging.aiyamassage.com/

Project Management
: https://trello.com/b/L9R8HKiF/
